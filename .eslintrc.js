module.exports = {
  overrides: [
    {
      extends: ['./js'],
      files: ['*.js'],
    },
  ],
  root: true,
};
