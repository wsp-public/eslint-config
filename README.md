## установка модуля
Сначала нужно установить модуль через "npm i <путь-к-пакету/репозиторию>"

## подключение конфигов

### eslint
Пример конфига .eslintrc.js из корня сервиса.

    module.exports = {
      overrides: [
        {
          extends: ['@wsp/eslint-config/js'],
          files: ['*.js'],
        },
        {
          extends: ['@wsp/eslint-config/ts'],
          files: ['*.ts'],
          parser: '@typescript-eslint/parser',
          parserOptions: {
            include: ['src/**/*.ts', 'test/**/*.ts', 'typings/**/*.ts'],
            project: './tsconfig.json',
          },
        },
      ],
      root: true,
    };

### prettier
добавить в `package.json` строчку

    "prettier": "@wsp/eslint-config"

